const btncalcular = document.getElementById('btncalcular');

btncalcular.addEventListener('click', function(){
    let altura = document.getElementById('altura').value;
    let peso = document.getElementById('peso').value;
    
    let resultado=peso/Math.pow(altura,2);

    document.getElementById('result').innerHTML="Tu IMC es: "+resultado.toFixed(2);
});