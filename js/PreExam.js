//diseñe un script que nonde se codifica se muestre la tabla usando imagenes como muestra

function mostrarTabla() {
    var select = document.getElementById('cmbTablas');
    var tabla = document.getElementById('tablaMultiplicacion');
    tabla.innerHTML = ''; // Limpiar tabla antes de generar una nueva

    var numeroTabla = parseInt(select.value);
    var tablaHTML = '<table>';

    for (var i = 1; i <= 10; i++) {
        tablaHTML += '<tr>';
        tablaHTML += '<td><img src="/img/' + numeroTabla + '.png" alt="' + numeroTabla + '"></td>';
        tablaHTML += '<td><img src="/img/x.png" alt="x"></td>';
        tablaHTML += '<td><img src="/img/' + i + '.png" alt="' + i + '"></td>';
        tablaHTML += '<td><img src="/img/=.png" alt="="></td>';
        var resultado = numeroTabla * i;
        if (resultado >= 10) {
            tablaHTML += '<td><img src="/img/' + Math.floor(resultado / 10) + '.png" alt="' + Math.floor(resultado / 10) + '"></td>';
            tablaHTML += '<td><img src="/img/' + (resultado % 10) + '.png" alt="' + (resultado % 10) + '"></td>';
        } else {
            tablaHTML += '<td><img src="/img/' + resultado + '.png" alt="' + resultado + '"></td>';
        }
        tablaHTML += '</tr>';
    }
    tablaHTML += '</table>';
    tabla.innerHTML = tablaHTML;
}