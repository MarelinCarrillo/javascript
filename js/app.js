//Obtener el objeto button de calcular

const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function(){
    let valorAuto =document.getElementById('valorAuto').value;
    let porcentaje = document.getElementById('porcentaje').value;
    let plazo = document.getElementById('plazos').value;
    
    //Hacer los calculos
    let pagoInicial = valorAuto * (porcentaje/100);
    let TotalFin = valorAuto - pagoInicial;
    let plazos= TotalFin/plazo;
    
    //Mostrar los datos
    
    document.getElementById('txtPagoInicial').value = pagoInicial;
    document.getElementById('totalFin').value = TotalFin;
    document.getElementById('pagoMensual').value = plazos;
});


btnLimpiar.addEventListener('click',function(){
    document.getElementById('txtPagoInicial').value="";
    document.getElementById('totalFin').value="";
    document.getElementById('pagoMensual').value="";
    document.getElementById('valorAuto').value="";
    document.getElementById('porcentaje').value="";
    document.getElementById('plazos').value="";
});